package com.dkr.ebanksvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EBankController {
	
	@GetMapping("/get")
	public String getMsg(){
		return "Welcome to E-Bank...";
	}

}
