package com.dkr.ebanksvc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.dkr.ebanksvc.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	Customer findById(long id);
	
	List<Customer> findByLastName(String lastName);
	
	@Query(value = "SELECT last_name AS LastName, count(last_name) AS Count FROM Customer Group By last_name", 
			  nativeQuery = true)
	List<List<String>> findAllActiveUsersNative();

}
