package com.dkr.ebanksvc;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.dkr.ebanksvc.entity.Customer;
import com.dkr.ebanksvc.repository.CustomerRepository;
import com.dkr.ebanksvc.service.EBankService;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class AppRunner 
{
	private static final Logger log = LoggerFactory.getLogger(AppRunner.class);
	
	@Autowired
	CustomerRepository repository;
	
	@Autowired
	EBankService bankService;
	
	
    public static void main( String[] args )
    {
        System.out.println( "Starting E-Bank.." );
        
        SpringApplication.run(AppRunner.class, args);
    }
    
    @Bean
    public CommandLineRunner demo() {
      return (args) -> {
        
    	bankService.transaction();  
    	  
    	/*// save a few customers
        repository.save(new Customer("Jack", "Bauer"));
        repository.save(new Customer("Chloe", "O'Brian"));
        repository.save(new Customer("Kim", "Bauer"));
        repository.save(new Customer("David", "Palmer"));
        repository.save(new Customer("Michelle", "Dessler"));

        // fetch all customers
        log.info("Customers found with findAll():");
        log.info("-------------------------------");
        for (Customer customer : repository.findAll()) {
          log.info(customer.toString());
        }
        log.info("");

        // fetch an individual customer by ID
        Customer customer = repository.findById(1L);
        log.info("Customer found with findById(1L):");
        log.info("--------------------------------");
        log.info(customer.toString());
        log.info("");

        // fetch customers by last name
        log.info("Customer found with findByLastName('Bauer'):");
        log.info("--------------------------------------------");
        repository.findByLastName("Bauer").forEach(bauer -> {
          log.info(bauer.toString());
        });
        // for (Customer bauer : repository.findByLastName("Bauer")) {
        //  log.info(bauer.toString());
        // }
        log.info("");
        
     // fetch customers by native query
        log.info("Customer found with findAllActiveUsersNative():");
        log.info("--------------------------------------------");
        repository.findAllActiveUsersNative().forEach((List<String> bauer) -> {
          log.info(bauer.get(0) + bauer.get(1));
        });*/
        
      };
    }
}
